package seeds

import (
	// "github.com/fteem/seeding/pkg/seed"
	"imp_case/seed"

	
	"github.com/jinzhu/gorm"

)

type SeedUser struct {
	gorm.Model
	Username string `gorm:"size:20;not null;unique" json:"username"`
	Fullname string `gorm:"size:50;not null;unique" json:"fullname"`
	Password string `gorm:"size:255;not null;" json:"password"`
}

func All() []seed.Seed {
	return []seed.Seed{}
}


func CreateUser(db *gorm.DB, username,fullname,password string) error {
	return db.Create(&SeedUser{Username: username, Fullname: fullname,Password:password}).Error
}