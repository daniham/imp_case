FROM golang:1.20-alpine

RUN apk update && apk add --no-cache git
# RUN go install github.com/cosmtrek/air@latest

RUN mkdir -p /app
WORKDIR /app
COPY go.mod /app
COPY go.sum /app
RUN go mod download

COPY . /app
RUN go mod tidy
# CMD ["air", "-c", ".air.toml"]

ENV TZ="Asia/Jakarta"
RUN go build -o ./app/imp_case

ENTRYPOINT ["./app/imp_case"]