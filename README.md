# Imp case API apps

## Architecture API

```
.
├── imp_case
│   	└── controllers
│   	└── middlewares
│   	└── models
│   	└── utils
│   		└── token
└── .env
└── .gitignore
└── docker-compose.yml
└── Dockerfile
└── go.mod
└── go.sum
└── main.go
└── README.md
```

# Config Database (Env)

1. Sesuaikan Config di file .env dengan koneksi masing-masing
2. Pastikan database telah di buat terlebih dahulu , misalnya **jwt_gin**.

# Installation 

## Menggunakan AIR (Direkomendasikan)

1. Instal cosmtrek/air(direkomendasikan) dengan mengikuti panduan pada https://github.com/cosmtrek/air#installation
3. Ketik `air` pada terminal/cmd untuk menjalankan Api.
4. API akan berjalan pada port 3030 secara default.
5. Sehingga API akan dapat diakses pada http://localhost:3030/{Endpoint}

## Menggunakan GO RUN

1. Ketik `go run main.go` pada terminal/cmd untuk menjalankan API.
2. API akan berjalan pada port 3030 secara default.
3. Sehingga API akan dapat diakses pada http://localhost:3030/{Endpoint}

## Menggunakan Docker Compose

1. Build API dengan mengetik `docker compose up -d` pada terminal/cmd.
2. Sehingga API akan dapat diakses pada http://localhost:3030/{Endpoint}



## Menggunakan Docker

1. Install Docker portainer dan gunakan port 9000 sebagai defaultnya ,ubuntu di link https://docs.fuga.cloud/how-to-install-portainer-docker-ui-manager-on-ubuntu-20.04-18.04-16.04
   dan untuk windows di link https://docs.portainer.io/start/install/server/docker/wcs
3. Build API menjadi docker file dengan mengetik `sudo docker image build -t imp_case .` pada terminal/cmd.
4. Setelah proses build berhasil, waktunya untuk menjalankan image dari API menjadi container dengan mengetik `sudo docker container run --name imp_case -p 3030:3030 -d imp_case` pada terminal/cmd.
5. Maka otomatis API akan berjalan pada port 3030 dan dapat diakses pada  http://localhost:3030/{Endpoint}.

