package main

import (
  	"github.com/gin-gonic/gin"
	"imp_case/controllers"
	"imp_case/models"
	"imp_case/middlewares"
)

func main() {
	models.ConnectDataBase()
	
	r := gin.Default()

	public := r.Group("/auth")

	public.POST("/signup", controllers.Register)
	public.POST("/login",controllers.Login)

	protected := r.Group("/user")
	protected.Use(middlewares.JwtAuthMiddleware())
	protected.GET("/userlist",controllers.CurrentUser)

	r.Run(":3030")
}